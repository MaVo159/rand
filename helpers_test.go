// Copyright 2014 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package rand

import (
	"reflect"
	"testing"
)

func testSequence(t *testing.T, source Source, sequenceTable []Sequence) {
	for si, sequence := range sequenceTable {
		if sequence.SeedSlice != nil {
			sourceSL := source.(interface {
				SeedSlice([]uint64)
			})
			sourceSL.SeedSlice(sequence.SeedSlice)
		} else {
			source.Seed(sequence.Seed)
		}
		for ri, r := range sequence.Sequence {
			if v := source.Uint64(); r != v {
				t.Errorf("%s: Mismatch in sequence %d at position %d (%d!=%d)", reflect.TypeOf(source).String(), si, ri, r, v)
				break
			}
			break
		}
	}
}

type Sequence struct {
	Seed      uint64
	SeedSlice []uint64
	Sequence  []uint64
}
