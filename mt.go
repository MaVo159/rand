// Copyright 2014 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// T. Nishimura:
// "Tables of 64-bit Mersenne Twisters"
// "ACM Transactions on Modeling and Computer Simulation 10. (2000) 348--357."

// M. Matsumoto and T. Nishimura:
// "Mersenne Twister: a 623-dimensionally equidistributed uniform pseudorandom number generator"
// "ACM Transactions on Modeling and Computer Simulation 8. (Jan. 1998) 3--30."

// This mersenne twister implementation is a translation of the C implementation
// by Takuji Nishimura and Makoto Matsumoto, available at
// http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/VERSIONS/C-LANG/mt19937-64.c,
// which is accompanied by the following notes, copyright notice and disclaimer:

/*    A C-program for MT19937-64 (2004/9/29 version).
Coded by Takuji Nishimura and Makoto Matsumoto.

This is a 64-bit version of Mersenne Twister pseudorandom number
generator.

Before using, initialize the state by using init_genrand64(seed)
or init_by_array64(init_key, key_length).

Copyright (C) 2004, Makoto Matsumoto and Takuji Nishimura,
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

  1. Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.

  2. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.

  3. The names of its contributors may not be used to endorse or promote
     products derived from this software without specific prior written
     permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package rand

// Mersenne Twister random number source (MT19937-64)
// A very popular random number generator, suitable for Monte Carlo simulations.
type MT19937_64 struct {
	state [312]uint64
	index int
}

// NewSource returns a new Mersenne Twister Source seeded with the given value.
func NewMersenneTwister(seed uint64) *MT19937_64 {
	var rng MT19937_64
	rng.Seed(seed)
	return &rng
}

// Seed uses the provided slice of seed values to initialize the Mersenne Twister to a deterministic state.
func (mt *MT19937_64) SeedSlice(seed []uint64) {
	mt.Seed(19650218)
	var i int = 1
	var k, j int
	if len(mt.state) > len(seed) {
		k = len(mt.state)
	} else {
		k = len(seed)
	}
	for ; k > 0; k-- {
		mt.state[i] = (mt.state[i] ^ ((mt.state[i-1] ^ (mt.state[i-1] >> 62)) * 3935559000370003845)) + seed[j] + uint64(j)
		i++
		j++
		if i >= len(mt.state) {
			mt.state[0] = mt.state[len(mt.state)-1]
			i = 1
		}
		if j >= len(seed) {
			j = 0
		}
	}
	for k = len(mt.state) - 1; k > 0; k-- {
		mt.state[i] = (mt.state[i] ^ ((mt.state[i-1] ^ (mt.state[i-1] >> 62)) * 2862933555777941757)) - uint64(i)
		i++
		if i >= len(mt.state) {
			mt.state[0] = mt.state[len(mt.state)-1]
			i = 1
		}
	}
	mt.state[0] = 1 << 63 /* MSB is 1; assuring non-zero initial array */
}

// Seed uses the provided seed value to initialize the Mersenne Twister to a deterministic state.
func (mt *MT19937_64) Seed(seed uint64) {
	mt.state[0] = seed
	for i := 1; i < len(mt.state); i++ {
		mt.state[i] = (6364136223846793005*(mt.state[i-1]^(mt.state[i-1]>>62)) + uint64(i))
	}
	mt.index = len(mt.state)
}

// Uint64 returns a non-negative pseudo-random 64-bit integer as an uint64.
func (mt *MT19937_64) Uint64() uint64 {
	const MM = 156
	var UM uint64 = 0xFFFFFFFF80000000 //Upper 33 bits
	var LM uint64 = 0x7FFFFFFF         //Lower 31 bits
	var mag01 [2]uint64 = [2]uint64{0, 0xB5026F5AA96619E9}
	var x uint64
	if mt.index >= len(mt.state) {
		var i int
		for ; i < len(mt.state)-MM; i++ {
			x = (mt.state[i] & UM) | (mt.state[i+1] & LM)
			mt.state[i] = mt.state[i+MM] ^ (x >> 1) ^ mag01[int(x&1)]
		}
		for ; i < len(mt.state)-1; i++ {
			x = (mt.state[i] & UM) | (mt.state[i+1] & LM)
			mt.state[i] = mt.state[i+(MM-len(mt.state))] ^ (x >> 1) ^ mag01[int(x&1)]
		}
		x = (mt.state[len(mt.state)-1] & UM) | (mt.state[0] & LM)
		mt.state[len(mt.state)-1] = mt.state[MM-1] ^ (x >> 1) ^ mag01[int(x&1)]
		mt.index = 0
	}
	x = mt.state[mt.index]
	mt.index++
	x ^= (x >> 29) & 0x5555555555555555
	x ^= (x << 17) & 0x71D67FFFEDA60000
	x ^= (x << 37) & 0xFFF7EEE000000000
	x ^= (x >> 43)
	return x
}
