This is a fork of the original go math/rand package whith the following features:

- Use 64bit sources of random numbers (instead of 63bit)
- Add a `Uint64()` method to the Rand type
- Add a Mersenne Twister source.

With the exception of the Uint63()->Uint64() conversion for sources, all code that works with the original package does not require any changes to work with this package and produces the same stream of random numbers.

You can import it using the following import statement:

`import "bitbucket.org/MaVo159/rand"`

Use `go get .` before building to automatically download this and other packages you use in your go project (if you have set the GOPATH environment variable).

The Mersenne Twister source was ported from C to Go using the C version of MT19937-64 by Takuji Nishimura and Makoto Matsumoto.

I try to modify the original files of the math/rand package as little as possible (keep the diff short) and apply/port all changes/fixes in Go's original math/rand package to this package.

Bug reports and suggestions via email or issue tracker are very welcome.

